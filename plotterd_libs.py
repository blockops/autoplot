#!/usr/bin/env python3

import os
import shutil
import psutil
import json
import pdb
import time
from typing import List, Union
import re
from datetime import datetime
import numpy
import itertools
import shlex

## queue
import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler

## watchdog
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

# must run . ./activate
class Humanize:
    METRIC_LABELS: List[str] = ["B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
    BINARY_LABELS: List[str] = ["B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"]
    PRECISION_OFFSETS: List[float] = [0.5, 0.05, 0.005, 0.0005] # PREDEFINED FOR SPEED.
    PRECISION_FORMATS: List[str] = ["{}{:.0f} {}", "{}{:.1f} {}", "{}{:.2f} {}", "{}{:.3f} {}"] # PREDEFINED FOR SPEED.

    @staticmethod
    def format(num: Union[int, float], metric: bool=False, precision: int=1) -> str:
        """
        Human-readable formatting of bytes, using binary (powers of 1024)
        or metric (powers of 1000) representation.
        print(Humanize.format(2251799813685247)) # 2 pebibytes
        print(Humanize.format(2000000000000000, True)) # 2 petabytes
        print(Humanize.format(1099511627776)) # 1 tebibyte
        print(Humanize.format(1000000000000, True)) # 1 terabyte
        print(Humanize.format(1000000000, True)) # 1 gigabyte
        print(Humanize.format(4318498233, precision=3)) # 4.022 gibibytes
        print(Humanize.format(4318498233, True, 3)) # 4.318 gigabytes
        print(Humanize.format(-4318498233, precision=2)) # -4.02 gibibytes
        """

        assert isinstance(num, (int, float)), "num must be an int or float"
        assert isinstance(metric, bool), "metric must be a bool"
        assert isinstance(precision, int) and precision >= 0 and precision <= 3, "precision must be an int (range 0-3)"

        unit_labels = Humanize.METRIC_LABELS if metric else Humanize.BINARY_LABELS
        last_label = unit_labels[-1]
        unit_step = 1000 if metric else 1024
        unit_step_thresh = unit_step - Humanize.PRECISION_OFFSETS[precision]

        is_negative = num < 0
        if is_negative: # Faster than ternary assignment or always running abs().
            num = abs(num)

        for unit in unit_labels:
            if num < unit_step_thresh:
                # VERY IMPORTANT:
                # Only accepts the CURRENT unit if we're BELOW the threshold where
                # float rounding behavior would place us into the NEXT unit: F.ex.
                # when rounding a float to 1 decimal, any number ">= 1023.95" will
                # be rounded to "1024.0". Obviously we don't want ugly output such
                # as "1024.0 KiB", since the proper term for that is "1.0 MiB".
                break
            if unit != last_label:
                # We only shrink the number if we HAVEN'T reached the last unit.
                # NOTE: These looped divisions accumulate floating point rounding
                # errors, but each new division pushes the rounding errors further
                # and further down in the decimals, so it doesn't matter at all.
                num /= unit_step

        return Humanize.PRECISION_FORMATS[precision].format("-" if is_negative else "", num, unit)
    
    @staticmethod
    def time_ago(time=False):
      """
      Get a datetime object or a int() Epoch timestamp and return a
      pretty string like 'an hour ago', 'Yesterday', '3 months ago',
      'just now', etc
      Modified from: http://stackoverflow.com/a/1551394/141084
      """
      now = datetime.now()

      if type(time) is int:
          diff = now - datetime.fromtimestamp(time)
      elif type(time) is float:
          diff = now - datetime.fromtimestamp(time)
      elif isinstance(time,datetime):
          diff = now - time
      elif not time:
          diff = now - now
      else:
          raise ValueError('invalid date %s of type %s' % (time, type(time)))
      second_diff = diff.seconds
      day_diff = diff.days

      if day_diff < 0:
          return ''

      if day_diff == 0:
          if second_diff < 10:
              return "just now"
          if second_diff < 60:
              return str(second_diff) + " seconds ago"
          if second_diff < 120:
              return  "a minute ago"
          if second_diff < 3600:
              return str( round(second_diff / 60, 0) ) + " minutes ago"
          if second_diff < 7200:
              return "an hour ago"
          if second_diff < 86400:
              return str( round(second_diff / 3600,1) ) + " hours ago"
      if day_diff == 1:
          return "Yesterday"
      if day_diff < 7:
          return str(day_diff) + " days ago"
      if day_diff < 31:
          return str(round(day_diff/7, 2)) + " weeks ago"
      if day_diff < 365:
          return str(round(day_diff/30,2)) + " months ago"
      return str(round(day_diff/365,2)) + " years ago"


class PlotterD:
  
  TEMP_PLOT_DIR = '/chiaplots'
  FINAL_PLOT_DIR = '/chia_saved_plots'
  KSIZE = 32
  POOL_KEY = 'a069c08f65a522dee06fb003f30df6212745154bdfea7c5604a7457bf746289aaaa64ac1b1bf05d7860b936b646d74f1'
  FARMER_KEY = '8758c7891532d3eb4bc124cb6cbb7b53d5c83ab96eaa4860ea496de8d9f18a5cf9639e0041b4e988ae98f981ed0bd1c9'
  PLOT_REGEX = 'plot-(k\d{2})-(\d{4}-\d{2}-\d{2}-\d{2}-\d{2})-(\w+).plot'
  TMP_PLOT_BUCKET_REGEX = 'plot-(k\d{2})-(\d{4}-\d{2}-\d{2}-\d{2}-\d{2})-(\w+).plot\.?(\w\d\w?)?\.?(\w\d)?\.?([\w\d]+)?.tmp'


  def __init__(self, job_list={}):
    self.systemInfo = self.getSystemInfo()

  def thread_count(self):
    return 2

  def number_of_plots(self):
    return 1 

  def memory(self):
    return 3380

  def compute_bucket_numbers(self):
    # more buckets mean faster utilization of temp disk,
    # more threads mean faster computation and more memory?
    return 128

  def parallelSize(self):
    # if this is queued than there is no reason to set other than 1 since
    # the service will always run once complete
    # if we use parallel plots then this number will change
    return 1

  def available_disk_space(self, systemData):
    # check if there is k32 = 109 GB available
    # count current jobs running and count those first before 
    # checking 
    return True

  def available_cpu_load(self, systemData):
    return systemData['cpu_total_percent'] <= 60

  def available_disk_load(self, systemData):
    return True

  def is_memory_available(self, systemData):
    return True

  def is_command_runnable(self):
    systemData = self.getSystemInfo()
    return self.available_cpu_load(systemData) and self.available_disk_space(systemData) and self.available_disk_load(systemData) and self.is_memory_available(systemData)
 
  # def disk_stats(self, process_id, device_path):
  #   psutil.Process(1)
  #   psutil.Process(process_id).io_counters()
  #https://stackoverflow.com/questions/49357887/how-to-get-current-disk-io-and-network-io-into-percentage-using-python
#    cat \
# >     <(cat /sys/block/sda/stat && cat /proc/uptime)            \
# >     <(sleep 1 && cat /sys/block/sda/stat && cat /proc/uptime) \
# >     | awk -v RS="" '{printf "%.2f%\n", ($27-$10)/($33-$16) / 10}';

  def getSystemInfo(self):
    system = {
        'boot_time': psutil.boot_time(),
        'cpu_count': psutil.cpu_count(),
        'cpu_total_percent': psutil.cpu_percent(interval=1, percpu=False),
        'cpu_percent': psutil.cpu_percent(interval=1, percpu=True),
        'disk_io_counters': psutil.disk_io_counters()._asdict(),
        'disk_usage': {},
        'swap_memory': psutil.swap_memory()._asdict(),
        'virtual_memory': psutil.virtual_memory()._asdict(),
        'loadavg': os.getloadavg()
    }

    partitions = psutil.disk_partitions()
    for p in partitions:
        try:
            usage = psutil.disk_usage(p.mountpoint)
            system['disk_usage'][p.mountpoint] = {
                'mountpoint': p.mountpoint,
                'total': usage.total,
                'used': usage.used,
                'percent': usage.percent
            }
        except PermissionError as e:
            next
    # update variable?        
    self.systemInfo = system
    return system

  def createPlot(self, **kwargs):
    value = f"screen -dmS chia_plot /usr/bin/chia plots create -f {self.FARMER_KEY} -p {self.POOL_KEY} -t {self.TEMP_PLOT_DIR} -d {self.FINAL_PLOT_DIR} -k {self.KSIZE} -n {self.number_of_plots()} -b {self.memory()} -r {self.thread_count()}"
    command_args = shlex.split(value)
    return command_args

  def dumpJson(self, indent=2):
    data = self.systemInfo
    return json.dumps(data, indent=indent)

  @staticmethod
  def extractMetaData(filePath, plot_regex=re.compile(PLOT_REGEX)):
    # TODO don't compile regex everytime, store elsewhere
    file_name = os.path.basename(filePath)
    match = plot_regex.match(file_name)
    finished = datetime.fromtimestamp(os.path.getctime(filePath))
    start = datetime.strptime(match[2], '%Y-%m-%d-%H-%M')
    duration = finished - start
    file_id = match[3][0:9]
    return {
      'id': file_id,
      'ksize': match[1],
      #'path': filePath, 
      'start': start.strftime('%Y-%m-%d %H-%M-%S'),
      'end': finished.strftime('%Y-%m-%d %H-%M-%S'),
      'duration': round(duration.total_seconds()),
      'size': Humanize.format(os.path.getsize(filePath), True)
    }

  @staticmethod
  def extractTempMetaData(filePath, plot_regex=re.compile(TMP_PLOT_BUCKET_REGEX)):
    # TODO don't compile regex everytime, store elsewhere
    file_name = os.path.basename(filePath)
    match = plot_regex.match(file_name)
    if not match:
      pdb.set_trace()
    finished = datetime.fromtimestamp(os.path.getctime(filePath))
    start = datetime.strptime(match[2], '%Y-%m-%d-%H-%M')
    duration = finished - start
    file_id = match[3][0:9]
    return {
      'id': file_id,
      'ksize': match[1],
      'start': start.strftime('%Y-%m-%d %H-%M-%S'),
      'end': finished.strftime('%Y-%m-%d %H-%M-%S'),
      'bucket': match[6].replace('sort_bucket_', ''),
      'thread': match[5],
      'parallel': match[4],
      'duration': round(duration.total_seconds()),
      'size': Humanize.format(os.path.getsize(filePath), True)
    }

  def topX(self, plots):
    return sorted(plots, key=lambda item: item['duration'])

  def byDate(self, plots):
    return sorted(plots, key=lambda item: item['start'])

class JobList():
  def __init__(self, entries={}):
    self.entries = entries

  def tempFiles(self):
    """
    @return [Array[Dict]] all the open files associated with plot creation
    """
    # on the 5th call it returns *** TypeError: 'NoneType' object is not subscriptable
    open_plot_files = []
    for proc in psutil.process_iter():
      # only for that user or all for root
      try:
        for f in proc.open_files():
          if 'plot-' in f.path:
            data = PlotterD.extractTempMetaData(f.path)
            data['pid'] = proc.pid

            open_plot_files.append(data)
      except psutil.AccessDenied as e:
        continue

    return open_plot_files

  def dead(self):
    # check finished
    # check tempfiles that have no processes
    pass

  def finished(self,path=PlotterD.FINAL_PLOT_DIR):
    regex = re.compile(PlotterD.PLOT_REGEX)
    plots = []
    # TODO don't walk, list entries
    for root, _, files in os.walk(path):
      for file in files:
        filePath = os.path.join(root, file)
        meta = PlotterD.extractMetaData(filePath, plot_regex=regex)
        plots.append(meta)

    return plots
  
  def running(self):
    # https://psutil.readthedocs.io/en/latest/
    groups = []
    uniquekeys = []

    data = sorted(self.tempFiles(), key=lambda i: i['id'])
    sorted_groups = itertools.groupby(data, key=lambda i: i['id'])
    for k, g in sorted_groups:
        group = list(g)
        groups.append(group)      # Store group iterator as a list
        proc = psutil.Process(group[0]['pid'])
        
        #t = datetime.fromtimestamp(proc.create_time()).strftime("%Y-%m-%d %H:%M:%S")
        uniquekeys.append({
          'id': k,
          'pid': proc.pid,
          'state': proc.status(),
          'started': Humanize.time_ago(proc.create_time()), 
          'cpu': proc.cpu_percent(.1)
        })
    
    return uniquekeys, groups

  def removeFromJobsList(self, path):
    print(f"on_created{filePath}")

class FileHandler(FileSystemEventHandler):
    def __init__(self, eventHandler):
      self.eventHandler = eventHandler

    def on_created(self, event, pattern):
      if re.match(pattern, event.src_plot):
        self.eventHandler(event.src_path)

