#!/usr/bin/env python3
"""Autoplot implementation using MILC.

PYTHON_ARGCOMPLETE_OK
"""
from milc import cli
from plotterd_libs import PlotterD
from plotterd_libs import JobList
from terminaltables import AsciiTable
import pdb

@cli.entrypoint('Autoplot chia blockchain tool')
def main(cli):
  cli.log.info('No subcommand specified!')
  cli.print_usage()

@cli.subcommand('Create a new job manually')
def addjob(cli):
  jobs, groups = JobList().running()
  p = PlotterD()
  if p.is_command_runnable():
    command_args = PlotterD().createPlot()
    output = cli.run(command_args, combined_output=True)
    cli.echo(output.stdout)
  else:
    cli.log.error('{bg_red}{fg_white}Not enough resources to create plot!')
  

@cli.subcommand('Show running jobs')
def running(cli):
  data = []
  jobs, groups = JobList().running()
  # initialize with key names for the labels
  data.append(['id', 'pid', 'state', 'started', 'cpu'])
  #data.append([*jobs[0].keys()])

  for job in jobs:
    data.append([*job.values()])

  t = AsciiTable(data)
  cli.echo(t.table)

# maybe have configs for by date, by duration, 
@cli.subcommand('Show finished jobs')
def finished(cli):
  data = []
  p = PlotterD()
  jobs = p.byDate(JobList().finished())
  # initialize with key names for the labels
  data.append([*jobs[0].keys()])

  for job in jobs:
    data.append([*job.values()])

  t = AsciiTable(data)
  cli.echo(t.table)

if __name__ == '__main__':
    cli()