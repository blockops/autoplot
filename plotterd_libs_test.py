from plotterd_libs import JobList
import pdb

def test_running():
    p = JobList()
    keys, groups = p.running()
    assert isinstance(keys, list)

def test_tmpFiles():
    p = JobList()
    assert isinstance(p.tempFiles(), list)

def test_finished():
    p = JobList()
    assert isinstance(p.finished(), list)