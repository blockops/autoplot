file { '/chia_temp_ssd':
  ensure   => 'directory',
  group    => 'opselite',
  mode     => '0755',
  owner    => opselite,
}
file{'/usr/bin/chia':
  ensure => link,
  target => '/usr/lib/chia-blockchain/resources/app.asar.unpacked/daemon/chia',
  require => Package['chia-blockchain']
}
file{'/opt/downloads': ensure => directory}

file{'/opt/downloads/chia-blockchain_1.1.2_amd64.deb':
  ensure => present,
  source => 'https://github.com/Chia-Network/chia-blockchain/releases/download/1.1.2/chia-blockchain_1.1.2_amd64.deb',
  require => File['/opt/downloads']
}
package{'chia-blockchain':
  ensure => present,
  source => '/opt/downloads/chia-blockchain_1.1.2_amd64.deb',
  require => File['/opt/downloads/chia-blockchain_1.1.2_amd64.deb']
}
